// TODO: arithmetic operations
/**
 * Adds two numbers together.
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b) => a + b;

/**
 * Subtracts a number from the minuend
 * @param {number} minuend 
 * @param {number} subtrahend 
 * @returns {number}
 */
const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

/**
 * Multiplies given multiplicant with given multiplier
 * @param {number} multiplier 
 * @param {number} multiplicant 
 * @returns {number}
 */
const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
};

/**
 * Divides two numbers.
 * @param {number} divident 
 * @param {number} divisor
 * @returns {number}
 * @throws {Error} 0 division 
 */
const divide = (divident, divisor) => {
    if(divisor == 0) throw new Error("0 division not allowed");
    const fraction = divident/divisor;
    return fraction;
}

/**
 * Returns the modulo of a division
 * @param {number} divident 
 * @param {number} divisor 
 * @returns {number}
 * @throws {Error} 0 division
 */
const modulo = (divident, divisor) => {
    if(divisor == 0) throw new Error("0 division not allowed");
    const modulo = divident % divisor;
    return modulo;
}

export default { add, subtract, divide, multiply, modulo }
