import { assert, expect, should } from "chai";
import { describe } from "mocha";
import calc from "../src/calc.js";


describe("Calc.js", () => {
    let myVar = 0;
    before(() => {
        myVar = 0;
    });
    beforeEach(() => myVar++);
    after(()=> console.log(`tests ran: ${myVar}`));

    it("can run basic adding and subtracting functions", () => {
        expect(calc.add(2,5)).to.equal(7);
        expect(calc.subtract(6,2)).to.equal(4);
    });
    it("can multiply and divide numbers", ()=> {
        should().exist(calc.multiply);
        expect(calc.multiply(2, -5)).to.equal(-10);
        should().exist(calc.divide);
        expect(calc.divide(6,2)).to.equal(3);
    });
})