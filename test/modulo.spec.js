import { assert, expect, should } from "chai";
import { describe } from "mocha";
import calc from "../src/calc.js";

describe("Calc.js", () => {
    it("can figure the modulo of a division", () => {
        should().exist(calc.modulo);
        expect(calc.modulo(5,3)).to.equal(2);
        assert(calc.modulo(6,3) == 0);
    });
});